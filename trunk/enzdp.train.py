import os, sys
import subprocess, signal
import datetime

from myutils import *
from time import sleep

ENZDP_ROOT = os.path.abspath(sys.path[0])
ENZPRO_WD = os.path.join(ENZDP_ROOT, "source", "train")

class EnzDP(object):
    def __init__(self,):
        #self.cfg = cfg
        self.EnzProTrainScript = os.path.join(ENZPRO_WD, "_1-mineSP.sh")

    def __enter__(self):
        return self
    def __exit__(self, *kwargs):
        logging.info("exiting\n")
        pass

    def run(self):
        logging.info("Started.")
        subPID = ""
        try:
            os.chdir(ENZPRO_WD)       
            command = ["sh" , self.EnzProTrainScript] 
            popen = subprocess.Popen(command, bufsize=0, stdout=subprocess.PIPE, stdin=subprocess.PIPE, preexec_fn=os.setsid)
            subPID = popen.pid
            logging.info("Called perl with pid: [%s]" % subPID)
            logging.info("Command: %s" % command)
            lines_out = iter(popen.stdout.readline, '')
            for line in lines_out:
                if line.strip():
                    logging.info(line.rstrip())
        except:
            logging.critical("Running failed:\n"+traceback.format_exc())
            killProcess(subPID, "Killed process: ")
        finally:
            sleep(1)
            killProcess(subPID, "Finally killed process: ")
        logging.info("Stoped.")

if __name__ == "__main__":
    initLog("Train.log")
    with EnzDP() as enzdpRunner:
        enzdpRunner.run()

