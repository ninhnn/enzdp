import os, sys, logging, traceback, signal

def initLog(logFile):
    logging.basicConfig(filename=logFile, format='%(asctime)s %(levelname)-8s: %(message)s', level=logging.INFO)
    #print "Log file will be [%s]" %logFile


def getConfig(PROJ_FILE):
    cfg = {}
    try:
        execfile(PROJ_FILE, cfg)
    except:
        logging.critical("Can't load project file [%s]" %PROJ_FILE)
        logging.error(traceback.format_exc())
        raise Exception("Failed")
    return cfg

def killProcess(subPID, msg):
    if subPID:
        os.killpg(os.getpgid(subPID), signal.SIGTERM)
        logging.warn("%s [%s]" %(msg, subPID))

def createDir(*aDir):
    aDir = os.path.join(*aDir)
    if not os.path.exists(aDir):
        os.makedirs(aDir)
    return aDir
