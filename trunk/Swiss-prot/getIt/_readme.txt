
This folder contains scripts to get latest data from Uniprot.

Check what is the latest release date of Swissprot:
    sh checkRelease.sh
    
To get the data:
    sh getIt.sh

