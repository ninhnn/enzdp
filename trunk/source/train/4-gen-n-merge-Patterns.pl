#! perl -w
#==============================================================================
# This scripts generates list of patterns for each EC
#>1.1.1.28	2
#1.1.1.28	1	PF00389|PF02826|
#1.1.1.28	2	PF01565|PF09330|
#1.1.1.28	2	PF01565|PF09330|PF0933x|
#
# Merging: For each ec number:
# 
# Two patterns will be merged if the weighted Jaccard score of them is greater than some threshold:
#
# PatScore(Pat) 				= SUM ASScore(ec,fz) for all fz belongs to pattern Pat
# CommonScore(Pat1, Pat2) 		= SUM ASScore(ec,fz) for all fz belongs to both patterns Pat1 and Pat2 
# weighted_Jaccard(Pat1, Pat2)	= 2* CommonScore (Pat1, Pat2) / ( PatScore(Pat1) + PatScore(Pat1) );
#
# mergeAble(Pat1, Pat2) = weighted_Jaccard(Pat1, Pat2) >= THRESHOLD;
#
#==============================================================================

$MERGE_ABLE_THRESHOLD 	= 0.75; 	# 0.8333 = 5/6; 0.8 = 4/5; 0.75 = 3/4;
$COMMON_THRESHOLD		= 0.75;

$TINY = 0.000000001;

#===INPUT===#
$DataDir = "../../data";
$ecDomFile = "$DataDir/EC.domains.tab";
#$ecPFscoreFile = "$DataDir/AS-Score/ECPF.ass.nor";	# normalized association score
$ecPFscoreFile = "$DataDir/AS-Score/ECPF.ass.raw";	# raw association score

#===OUTPUT===#
$outFile = "$DataDir/EC-sgr-patterns.tab";

#=================================================
# Read EC domains file:
#2.1.1.189	PF01938|PF05958|	2	1	459.00	0.00	0.00
#2.1.1.189	PF03848|PF05958|	2	2	411.00	1.41	1.00
#2.1.1.189	PF05958|	1	97	377.21	18.05	5.93
#2.1.1.190	PF01938|PF05958|	2	117	444.92	13.37	9.38
%LINEsOfEC = ();
open(FILE,$ecDomFile)||die;
while(<FILE>){
	($ec) = split(/\s+/,$_);
	#next if ($ec =~ /\.-\.-/);
	next if ($ec =~ /9\.9\.9\.9/);
	if (exists $LINEsOfEC{$ec}){
		$LINEsOfEC {$ec} = $LINEsOfEC {$ec} . $_;
	} else {		$LINEsOfEC {$ec} = $_;};
}
close(FILE);

#=================================================
# Read domain-EC association score file:
%ScoreOfPair = ();
open(FILE,$ecPFscoreFile) || die "no association score file [$ecPFscoreFile]\n";
while(<FILE>){
	($ec,$pf, $score) = split;
	$ScoreOfPair{$ec."\t".$pf} = $score;
};
close(FILE);

#=================================================
#Generate list of patterns:
$nSGR = 0;
print "Writing file [$outFile]: \n";
open(FILE,">",$outFile);
foreach $myEC (keys %LINEsOfEC){
	@SGR = ();
	($total,@SGR) = mergeForAnEC($myEC, $LINEsOfEC{$myEC});
	print FILE ">". $myEC ."\t". $total . "\n";
	print FILE $myEC ."\t". $_ ."\n" for(@SGR);
	$nSGR += $total;
}
close(FILE);

print "There are [$nSGR] subgroups in total!\n";
print "Writing file [$outFile]: DONE\n\n";
### END

#########################################
#########################################
sub mergeForAnEC{
	local ($i, $j, $pat, @Pats, @Mark, @GRP, $nGrp);
	my $EC = shift;
	my $lines = shift;

	my @LINEs = split(/\n/,$lines);	

	@Pats = ();
	@Mark = ();
	@GRP = ();
	foreach (@LINEs){
		(undef,$pat) = split;
		push(@Pats, $pat);
		push(@Mark,0);
		push(@GRP ,0);	
	}

#Sort first:
	@Pats = sortStringArrDec(@Pats);

# Merging:
	$nGrp=0;
	for( $i = 0; $i<= $#Pats; $i++){
		next if ($Mark[$i] == 1);
		$nGrp++;
		$Mark[$i] = 1;
		$GRP[$i] = $nGrp;
		for( $j = $i+1; $j<= $#Pats; $j++){
			next if ($Mark[$j] == 1);
			if (mergeAble($EC, $Pats[$i], $Pats[$j]) == 1){
				$Mark[$j] = 1;
				$GRP[$j] = $GRP[$i];
			}; # if mergeAble
		}; # for j
	}; # for i
	
	# add grpID:
	for($i = 0; $i<= $#Pats; $i++){
		$Pats[$i] = $GRP[$i] ."\t". $Pats[$i];
	}; # for i

	# sorting:
	@Pats = sort @Pats;
# Merging done.

	return ($nGrp, @Pats);
}

#########################################
sub mergeAble{
	my $EC	= shift;
	return 0 if ($EC =~/ECm/);	
	my $pat1 = shift;
	my $pat2 = shift;

	my @Arr = ();

# pattern 1:
	@Arr = split(/\|/,$pat1);
	my %PFSet1 = ();
	$PFSet1{$_}++ foreach (@Arr);

	my $pat1Score = 0.0;
	foreach $PF (keys %PFSet1){
		 $pat1Score +=	$ScoreOfPair{$EC."\t". $PF}	if (exists $ScoreOfPair{$EC."\t". $PF});
	}

# pattern 2:
	@Arr = split(/\|/,$pat2);
	my %PFSet2 = ();
	$PFSet2{$_}++ foreach (@Arr);

	my $pat2Score = 0.0;
	foreach $PF (keys %PFSet2){
		 $pat2Score +=	$ScoreOfPair{$EC."\t". $PF}	if (exists $ScoreOfPair{$EC."\t". $PF});
	}

# common pattern:
	my $nCommon = 0;
	my $myCommonScore = 0;
	foreach $PF (keys %PFSet1){
	if (exists $PFSet2{$PF}){
		$myCommonScore += $ScoreOfPair{$EC."\t". $PF}	if (exists $ScoreOfPair{$EC."\t". $PF});
		$nCommon++;
	}};

# Jaccard score:
	#print "$EC\t$pat1\t$pat2\n"	if (($pat1Score + $pat2Score) == 0.0);
	$myJaccardScore = 2 * $myCommonScore / ($pat1Score + $pat2Score + $TINY);	# fixed division by zero Jan-16


# check common dom ratio:
	my $nPF1 = scalar keys %PFSet1;
	my $nPF2 = scalar keys %PFSet2;

	my $ratio = 2*$nCommon / ($nPF1 + $nPF2  + $TINY);

# Return:
	my $good = 0;	
	$good = 1 if ( ($myJaccardScore >= $MERGE_ABLE_THRESHOLD) and ($ratio >= $COMMON_THRESHOLD) );

	return $good;

};
#########################################

sub sortStringArrDec{	# decreasing by length first
	my @Arr = @_;
	@Arr = reverse sort {
		if (length($a) == length($b)) {return ($a cmp $b) }
		else { return (length($a) <=> length($b)) };
	} @Arr; # reverse of acsending
	return @Arr;
};


=comment Test sorting:
@Arr = ("abc","abd", "e", "ab", "ac", "aaaa", "abab");
@Arr = sortStringArrDec(@Arr);
print $_ ."\n" foreach(@Arr);
exit(0);
=cut



