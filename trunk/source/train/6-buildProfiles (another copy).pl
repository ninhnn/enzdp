#! perl -w
#==============================================================================
#
#==============================================================================

use Parallel::ForkManager;
my $maxProcs = 6;
my $pm = new Parallel::ForkManager($maxProcs);


#===INPUT===#
$DataDir 	= "../../data";
$SGRlistFile = "$DataDir/SGR.list";
$SGRDir = "../../SGRs";
$uniprotFasta 	=  "../../Swiss-prot/SwissProt.fasta";

# Dependent scripts:
$hmmBuild_Script 		= "build_MSA_HMM.pl";
$hmmSearch_Script		= "searchHMM.pl";
$calcThreshold_Script	= "calc_Threshold_SGR.pl";
$findSite_Script		= "FindSites.pl";

#===OUTPUT===#
$msaDir = "../../MSAs";
system("mkdir -p $msaDir");
system("mkdir -p ../log");
$hmmDir = "../../HMMs";
system("mkdir -p $hmmDir");

#$HMMthresholdsFile = "$DataDir/HMM.thresholds";

#=======================================
# Read the list of SGRs:
#4.2.3.19.sgr1
#ECm_332.sgr1
%SGRSet = ();
open(FILE, $SGRlistFile) || die "no [$SGRlistFile] file\n";
while(<FILE>){
	chomp($_);	
	($sgr) = split(/\s+/,$_);
	$SGRSet{$sgr}++;	
}
close(FILE);
@SGRList = ();
push(@SGRList,$_) for(keys %SGRSet);

#=======================================
# Run for each SGR:
$total = scalar keys %SGRSet;
$n = 0;
$startTime = time;
#system ("date");
#unlink($HMMthresholdsFile);



#build first:
#=============
print "BUILDING MSA and HMM:\n";
foreach $sgr (@SGRList){	
	$n++;
	$pm->start and next;

	$elapsed = time - $startTime;
	system ("date");
	print "[$n / $total] Running for $sgr: [$elapsed seconds]                         \n"; # if ($n % 10 == 0);

	$fastaFile  = $SGRDir 		. "/$sgr.fasta";
	$msaFile	= $msaDir 	. "/$sgr.msa";
	$hmmFile	= $hmmDir 	. "/$sgr.hmm";

	#build MSA and HMM for this fasta file:
	print "\tBuilding MSA and HMM:\n";
	#system("perl $hmmBuild_Script $sgr $fastaFile $msaFile $hmmFile");
	print "\tBuilding MSA and HMM: DONE!\n";

	$pm->finish;
}
$pm->wait_all_children;

print "BUILDING MSA and HMM:	DONE\n\n";


$n = 0;
#Search
#==========:
print "SEARCHING PROFILES against Uniport:\n";
foreach $sgr (@SGRList){	
	$n++;
	$pm->start and next;

	$elapsed = time - $startTime;
	system ("date");
	print "[$n / $total] Running for $sgr: [$elapsed seconds]                         \n"; # if ($n % 10 == 0);

	$fastaFile  = $SGRDir 		. "/$sgr.fasta";
	$msaFile	= $msaDir 	. "/$sgr.msa";
	$hmmFile	= $hmmDir 	. "/$sgr.hmm";

	#search HMM against uniprot SP:
	print "\tSearch against uniprot:\n";
	$hmmSearchOut 		= $hmmDir . "/$sgr.hmm.search_out.sp";
	system("perl $hmmSearch_Script $hmmFile $uniprotFasta $hmmSearchOut");
	print "\tSearch against uniprot: DONE!\n";

	$pm->finish;
}
$pm->wait_all_children;
print "SEARCHING PROFILES against Uniport:	DONE\n\n";


$n = 0;
#Calc threshold
#==============
print "CALCULATING threshold:\n";
foreach $sgr (@SGRList){	
	$n++;
	$pm->start and next;

	$elapsed = time - $startTime;
	system ("date");
	print "[$n / $total] Running for $sgr: [$elapsed seconds]                         \n"; # if ($n % 10 == 0);

	$fastaFile  = $SGRDir 		. "/$sgr.fasta";
	$msaFile	= $msaDir 	. "/$sgr.msa";
	$hmmFile	= $hmmDir 	. "/$sgr.hmm";

	$hmmSearchOut 		= $hmmDir . "/$sgr.hmm.search_out.sp";

	#calc threshold for profile:
	print "\tCalculate optimal cut-off threshold:\n";
	$IdlistFile 	= $SGRDir 		. "/$sgr.Idlist";
	$thresholdFile 	= $hmmDir . "/$sgr.hmm.threshold";
	($ec_inp) = split(/\.sgr/,$sgr);
	system("perl $calcThreshold_Script $ec_inp $hmmSearchOut $IdlistFile $thresholdFile $sgr");
	print "\tCalculate optimal cut-off threshold: DONE!\n";

	$pm->finish;
}
$pm->wait_all_children;

print "CALCULATING threshold:	DONE\n\n";



$n = 0;
#Finding known sites:
#====================
print "FINDING known SITES:\n";
foreach $sgr (@SGRList){	
	$n++;
	$pm->start and next;

	$elapsed = time - $startTime;
	system ("date");
	print "[$n / $total] Running for $sgr: [$elapsed seconds]                         \n"; # if ($n % 10 == 0);

	$fastaFile  = $SGRDir 		. "/$sgr.fasta";
	$msaFile	= $msaDir 	. "/$sgr.msa";
	$hmmFile	= $hmmDir 	. "/$sgr.hmm";

	$hmmSearchOut 		= $hmmDir . "/$sgr.hmm.search_out.sp";

	$IdlistFile 	= $SGRDir 		. "/$sgr.Idlist";
	$thresholdFile 	= $hmmDir . "/$sgr.hmm.threshold";


	#Find active sites and any other specific sites:
	print "\tFinding known specific sites:\n";
	$acitveSiteFile 	= $hmmDir . "/$sgr.hmm.sites";
	unlink($acitveSiteFile);
	system("perl $findSite_Script $hmmSearchOut.raw $acitveSiteFile");
	print "\tFinding known specific sites: DONE!\n";

	$pm->finish;
}
$pm->wait_all_children;

print "FINDING known SITES:	DONE\n\n";




print "\n";
system ("date");

print "Total running time: ". (time - $startTime). " seconds\n";

### END

