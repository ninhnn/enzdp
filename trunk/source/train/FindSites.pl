#! /usr/bin/perl -w

#Analyse the hmm search output file to find list of known specific sites for the profile corresponding to each matched sequence

#===INPUT===#
$hmmSearchOutFile 	= $ARGV[0];
($hmmName) 	= split(/\.hmm\./,$hmmSearchOutFile);
@tmpArr 	= split(/\//,$hmmName);
$hmmName 	= $tmpArr[-1];
#print "hmmName = $hmmName\n";
($ecName) 	= split(/\.sgr/,$hmmName);
#print "ecName = $ecName\n";
#$SGRDir 			= "../../SGRs";

$knownSiteFile		= "../../data/spACID.sites";

#===OUTPUT===#
$OutFile 	=	$ARGV[1];


#=== Global vars===#

%AllKnownSitesOf= ();
%BestBSOf		= ();
%LinesOfHitID 	= ();
%ECMemSet 		= ();
%ECMemSetStrict	= ();
%ECRawOf 		= ();


@QueryStarts 	= ();
@QueryStrings 	= ();
@QueryEnds		= ();
@HitStarts 	= ();
@HitStrings 	= ();
@HitEnds		= ();

$queryName = $hmmName;

#=================================================
# read knownSite file:
%PairOf = ();
open(FILE,$knownSiteFile) || warn "File [$knownSiteFile] missing?\n";
while(<FILE>){
	chomp;
	($ecid, $id, $sites, $ecraw) = split(/\t/);
	next if (!($sites=~/\|/) );
	$AllKnownSitesOf{$id} = $sites;
	$ECRawOf{$ecid} = $ecraw;
	$PairOf{$id."\t".$ecid}++;
#	$ECMemSet{$id} ++ if (isSubSetEC($ecid, $ecName));
};
close(FILE);

exit if (! exists ($ECRawOf{$ecName}));

#=================================================
# read search out file
open(FILE,$hmmSearchOutFile) || warn "File [$hmmSearchOutFile] missing?\n";

$HitStartFlag = 0;
$myHitID = "InitHitIDName";
$lCnt = 0;
while(<FILE>){
	$lCnt++;
	next if ($lCnt < 16);
	if ($HitStartFlag == 0){
		next if (/inclusion threshold/);
		(undef, undef, undef, undef, $bestBS, undef, undef, undef, $spACID) = split;
		$BestBSOf{$spACID} = $bestBS if ($spACID);
	};

	if (/^Domain annotation for each sequence/){	# Start of hit description
		$HitStartFlag = 1;
		next;
	}
	next if ($HitStartFlag == 0);

	if (/^>>/){
		(undef, $myHitID) = split;
		next if (!$myHitID);
		$LinesOfHitID{$myHitID} = "";
	};
	next if (!exists $BestBSOf{$myHitID});
	$LinesOfHitID{$myHitID} = 	$LinesOfHitID{$myHitID} . $_;
}; #end reading file.
close(FILE);
#=================================================

foreach(keys %PairOf){
	($pid,$ecid) = split;

	$ECMemSetStrict{$pid} ++ if ($ecid eq $ecName);

	next if (! exists $LinesOfHitID{$pid});	

	$ECMemSet{$pid} ++ if ( (isSubSetEC($ECRawOf{$ecid}, $ECRawOf{$ecName})==1) or
							(isSubSetEC($ECRawOf{$ecName}, $ECRawOf{$ecid})==1) );
};

#Find sites for all member of the EC:
#====================================
%MemECSitesSet = ();

foreach $pid (keys %ECMemSetStrict){
	next if (!exists $LinesOfHitID{$pid});
	$ecSites = FindSitesForMemPID($pid);
	next if (! $ecSites);
	foreach (split(/\s+/,$ecSites)){
		$MemECSitesSet{$_}++ if ($_ !~ /NONE/);
	};
};
#print $_."\t".$MemECSitesSet{$_}."\n" foreach (keys %MemECSitesSet);

#=================================================
# checking the other hit if sites are also matched:
%NonMemSitesSet = ();
$NonMemSitesSet{$_} = 0 foreach(keys %MemECSitesSet);

foreach $pid (keys %LinesOfHitID){
	next if (exists $ECMemSet{$pid});	#consider only hit id that is not a member (false positive);
	#print "$pid\n";
	CheckSitesForNonMemPID($pid);
};
#print "Non-Mem:\n";
#print $_."\t".$NonMemSitesSet{$_}."\n" foreach (keys %NonMemSitesSet);


# Recount with sub set:
%AddMem = ();
%AddNonMem = ();
@Arr = keys %MemECSitesSet;
$n = $#Arr;
for my $i (0 .. $n){
for my $j (0 .. $n){
	next if ($i == $j);
	my $s1 = $Arr[$i];
	my $s2 = $Arr[$j];
	next if (isSubsetAS($s1, $s2) == 0);	# check if $s1 is subset of $s2
	# $s1 is subset of $s2:
	$AddMem{$s1} += $MemECSitesSet{$s2};		
	$AddNonMem{$s1} += $NonMemSitesSet{$s2};		

}}; # double for i for j

#re-count:
foreach(keys %MemECSitesSet){
	next if (!exists $AddMem{$_});
	$MemECSitesSet	{$_} += $AddMem	{$_};
	$NonMemSitesSet	{$_} += $AddNonMem {$_};		
};



#=================================================
# PRINTING OUTPUT FILE:
#======================

open (OUTPUT, ">",$OutFile);

foreach (keys %MemECSitesSet){
	print OUTPUT $_."\t". $MemECSitesSet{$_}. "\t". $NonMemSitesSet{$_} ."\n";
};
close(OUTPUT);

exit;

##################################################
##################################################
##################################################
#=================================================
sub isSubsetAS{
#MT:102:C|105:C|113:C|163:C|46:C|
#MT:102:C|105:C|113:C|163:C|46:C|68:H|99:C|
	local $a = shift;
	local $b = shift;
	$a = substr($a,3);
	$b = substr($b,3);
	$b = "|$b|";
	local @Arr = split(/\|/,$a);

	foreach $item(@Arr){
		return 0 if ($item =~/NONE/);	#NONE is not a subset of any set!
		return 0 if (index($b,"|$item|") < 0);
	};
	return 1;
};
#=================================================
sub CheckSites{
	my $sites = shift;	#	BD:87:R|193:Q|

#process the alignment:
#----------------------
	%HitCharOfPos		= ();
	for($i = 0; $i <=$#QueryStarts; $i++){
#        1.6.99.5.sgr21  12 illlelikGlgitlk.......tllkktvTleYPeekvelppryrGrhaltrredgkerCvaCylCataCPaqciyieaaeekdeagek 93  
#  sp|P0AFD8|NUOI_ECO57   1 MTLKELLVGFGTQVRsiwmiglHAFAKRETRMYPEEPVYLPPRYRGRIVLTRDPDGEERCVACNLCAVACPVGCISLQKAETKD--GRW 87 
#       2.7.11.1.sgr113 198 tlqktrarsklkseikihsslshenivkfehcfeneenvyillelcnqktvmdihkkrkylmeyetkyyvyqvimavqylhnnniihrd 286
#  sp|Q6CJA8|HOG1_KLULA   - -----------------------------------------------------------------------------------------   -

		next if ($HitStarts[$i] !~ /\d/);
		$posHit = $HitStarts[$i] - 1;
		$posQue = $QueryStarts[$i] - 1;

		for($j = 1; $j<=length($HitStrings[$i]); $j++){
			$hitChar = substr($HitStrings[$i],$j-1,1);
			$posHit++ if ($hitChar ne "-");
			$queChar = substr($QueryStrings[$i],$j-1,1);
			$posQue++ if ($queChar ne ".");

			$HitCharOfPos{$posQue} = $hitChar if ( ($hitChar ne "-") and ($queChar ne ".") ) ;
		}; #for j
	}; #for i
#process the alignment: DONE.
#----------------------------

# check each position:
	my @SiteArr = split(/:/,$sites);			#	BD:87:R|193:Q|
	for(my $i = 0; $i <=$#SiteArr; $i++){		
		my $pos = $SiteArr[$i];					# e.g. $pos = R|193
		next if (!($pos =~ /\d/)); 				# not matched to any digit (position)
		$pos =~ s/[^0-9]//g;					# replace all non-digit? $pos = 193;
		my $char = substr($SiteArr[$i+1],0,1); 	# $char = "Q";
		return 0 if (!exists $HitCharOfPos{$pos});
		return 0 if (uc($HitCharOfPos{$pos}) ne uc($char));
	}; # for i

	return 1;
}; # SUB


#=================================================
# new method for find sites:
#Find sites at profile, given the known site of the hit:
sub FindSites{
	my $sites = shift;	#known sites of the hit, e.g.: AS:234:D| BD:87:R|193:Q| MT:NONE CH:NONE

#process the alignment:
#---------------------
	%QueryPosOfHitPos = ();
	%QueryCharOfPos = ();
	for( $i = 0; $i <=$#QueryStarts; $i++){
#        1.6.99.5.sgr21  12 illlelikGlgitlk.......tllkktvTleYPeekvelppryrGrhaltrredgkerCvaCylCataCPaqciyieaaeekdeagek 93  
#  sp|P0AFD8|NUOI_ECO57   1 MTLKELLVGFGTQVRsiwmiglHAFAKRETRMYPEEPVYLPPRYRGRIVLTRDPDGEERCVACNLCAVACPVGCISLQKAETKD--GRW 87 
#       2.7.11.1.sgr113 198 tlqktrarsklkseikihsslshenivkfehcfeneenvyillelcnqktvmdihkkrkylmeyetkyyvyqvimavqylhnnniihrd 286
#  sp|Q6CJA8|HOG1_KLULA   - -----------------------------------------------------------------------------------------   -

		next if ($HitStarts[$i] !~ /\d/);
		$startHit = $HitStarts[$i] - 1;
		$startQue = $QueryStarts[$i] - 1;
		for($j = 1; $j<=length($HitStrings[$i]); $j++){
			$hitChar = substr($HitStrings[$i],$j-1,1);
			$startHit++ if ($hitChar ne "-");
			$queChar = substr($QueryStrings[$i],$j-1,1);
			$startQue++ if ($queChar ne ".");
			$QueryPosOfHitPos{$startHit} = $startQue 	if ( ($hitChar ne "-") and ($queChar ne ".") ) ;
			$QueryCharOfPos  {$startHit} = $queChar 	if ( ($hitChar ne "-") and ($queChar ne ".") ) ;
		};
	}; #for i
#process the alignment: DONE.
#----------------------------

# find corresponding position in profile of the known site:
	my @SiteArr = split(/:/,$sites); # AS:234:D| BD:87:R|193:Q| MT:NONE CH:NONE
	for(my $i = 0; $i <=$#SiteArr; $i++){
		my $pos = $SiteArr[$i];					# e.g. $pos = R|193
		next if (!($pos =~ /\d/)); 				# not matched to any digit (position)
		$pos =~ s/[^0-9]//g;					# replace all non-digit? $pos = 193;
		my $char = substr($SiteArr[$i+1],0,1); 	# $char = "Q";

		if ( (exists $QueryPosOfHitPos{$pos}) and (uc($char) eq uc($QueryCharOfPos{$pos})) ){ # Matched upper case
			$profilePos = $QueryPosOfHitPos{$pos};
			$SiteArr[$i] =~ s/\Q$pos\E/\Q$profilePos\E/g;			
		}
		else{# not-matched, replace by dummy -
			$SiteArr[$i] =~ s/\Q$pos\E/-/g;						
			$SiteArr[$i+1] = "-". substr($SiteArr[$i+1],1);
		};		
	};# for each pos

# form resulting sites:
	my $resSites = "";
	for(my $i = 0; $i <=$#SiteArr; $i++){
		$resSites = $resSites .$SiteArr[$i];
		$resSites = $resSites.":" if ($i <$#SiteArr);
	};

	$resSites =~ s/\Q-:-|\E//g;	# delete not-matched position

	return sortStandard($resSites);	#sort for hasing purpose!
}; # SUB new method for find sites!

#=================================================
sub sortStandard{
	my $str = shift;
	@Arr = split(/\s/,$str);
	my $Res = "";
	foreach $type(@Arr){
		$t = substr($type,0,3);
		$s = substr($type,3);
		next if (! $s);
		@SArr = split(/\|/,$s);
		@SArr = sort @SArr;
		$Res = $Res .$t;
		my $i = 0;
		foreach (@SArr){
			$item = $SArr[$i];
			next if (($i>0) and ($item eq $SArr[$i-1]));
			if ($s =~/NONE/){ 	$Res = $Res.$_;}
			else{				$Res = $Res.$_."|";}			
			$i++;
		}
		$Res = $Res ." ";
	};
	return $Res;
};

#=================================================
sub FindSitesForMemPID{
	my $id = shift;
	my $knownSites = $AllKnownSitesOf{$id};
	my $lines = $LinesOfHitID{$id};

	my @LINES = split(/\n/,$lines);

#reset everything:
	@QueryStarts 	= ();
	@QueryStrings 	= ();
	@QueryEnds		= ();
	@HitStarts 	= ();
	@HitStrings 	= ();
	@HitEnds		= ();
	
	$BestDomStartFlag = 0;
	$BestDomID = 0;
	foreach (@LINES){
		next if (/^>>/);
		if (/\!/){
			($domID,undef,$bs) = split;
			$BestDomID = $domID if ($bs == $BestBSOf{$id});
			next;
		};

		if (/== domain/){
			$BestDomStartFlag = 0;
			$BestDomStartFlag = 1 if (/== domain\s+\Q$BestDomID\E/);
		};

		next if ($BestDomStartFlag == 0);

		if (/\Q$queryName\E/){
			(undef,$start,$string,$end) = split;
			push(@QueryStarts, $start);		
			push(@QueryStrings, $string);		
			push(@QueryEnds, $end);	
		}	
		if (/\Q$id\E/){
			(undef,$start,$string,$end) = split;
			push(@HitStarts, $start);		
			push(@HitStrings, $string);		
			push(@HitEnds, $end);	
		};	
	}; #for each line

	my $resSites = FindSites($knownSites);
	return $resSites;
};

#=================================================
sub CheckSitesForNonMemPID{
	my $id = shift;
	my $lines = $LinesOfHitID{$id};

	my @LINES = split(/\n/,$lines);

#reset everything:
	@QueryStarts 	= ();
	@QueryStrings 	= ();
	@QueryEnds		= ();
	@HitStarts 	= ();
	@HitStrings 	= ();
	@HitEnds		= ();

	$BestDomStartFlag = 0;
	$BestDomID = 0;
	foreach (@LINES){
		next if (/^>>/);
		if (/\!/){
			($domID,undef,$bs) = split;
			$BestDomID = $domID if ($bs == $BestBSOf{$id});
			next;
		};

		if (/== domain/){
			$BestDomStartFlag = 0;
			$BestDomStartFlag = 1 if (/== domain\s+\Q$BestDomID\E/);
		};
		
		next if ($BestDomStartFlag == 0);	

		if (/\Q$queryName\E/){
			(undef,$start,$string,$end) = split;
			push(@QueryStarts, $start);		
			push(@QueryStrings, $string);		
			push(@QueryEnds, $end);	
		}	
		if (/\Q$id\E/){
			(undef,$start,$string,$end) = split;
			push(@HitStarts, $start);		
			push(@HitStrings, $string);		
			push(@HitEnds, $end);	
		};	
	}; #for each line

#check each member sites:
	foreach $memSite (keys %MemECSitesSet){			
		if (CheckSites($memSite) == 1){
			$NonMemSitesSet{$memSite}++;
		};
	}; # 
};
#=================================================
sub isSubSetEC{	# check whether (1st) $theECs is a sub-set of (2nd) $currECs:
	my $theECs	= shift;	# 1.2.9.-|3.3.6.4|
	my $currECs	= shift;	# 1.2.9.5|3.3.6.44|

	my @TMPArr = split(/\|/,$theECs);

	foreach $ec (@TMPArr){
		$ec =~ s/\Q.-\E//g;
#		print "test for $ec:\n";
		$match = 0;
		$match ++ if ($currECs =~ m/\Q$ec.\E/);
		$match ++ if ($currECs =~ m/\Q$ec|\E/);
		return 0 if ($match < 1);
#		print "test for $ec: GOOD!\n";;
	};

	return 1;
}


