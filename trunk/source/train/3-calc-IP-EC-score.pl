#! perl -w

#================================================================================
# This scripts calculates the association score between EC and InterPro signature
# in Swiss Prot database
#
# n(fy) 	= number of proteins carry the signature fy
# n(ex) 	= number of proteins that annotated by the EC number ex
# n(ex,fy) 	= number of proteins that carry signature fy and annotated by the EC 
# number ex
#
# Association score is calculated as: 
#					Sa(ex,fy) 	= 2*n(ex,fy) / ( n(ex) + n(fy) )
# It is normalized by:
#					Sna(ex,fy) 	= 2*Sa(ex,fy) / ( Max_Sa(ex) + Max_Sa(fy) )
# Where:
#	Max_Sa(ex) = max{ Sa(ex,fz): fz = [1 .. all signature]}
# 	Max_Sa(fy) = max{ Sa(ez,fy): ez = [1 .. all EC numbers]}
#================================================================================

#===INPUT===#
$DataDir = "../../data";
$infoFile = "$DataDir/spACID.ann";
#ECm_315 sp|Q6GZX4|001R_FRG3G 256 654924 PF04947| IPR007031| 9.9.9.9| AS:NONE DB:NONE MT:NONE CH:NONE
#ECm_315 sp|Q6GZX3|002L_FRG3G 320 654924 PF03003| IPR004251| 9.9.9.9| AS:NONE DB:NONE MT:NONE CH:NONE


#===OUTPUT===#
$OutDir = "$DataDir/AS-Score";
system("mkdir -p $OutDir");

$rawFile = "$OutDir/ECIP.ass.raw";
$norFile = "$OutDir/ECIP.ass.nor";

#=====================================
# Reading data:
%NumDo = ();
%NumEC = ();
%Npair = ();
open(FILE,$infoFile) || die "no input data\n";
while(<FILE>){
	chomp;
	next if (/^>/);
	next if (/^#/);

	(undef, undef,undef,undef,undef,$IPs,$ECs) = split; # IP signature is in 6th possition.

	next if ($IPs =~ /NONE/);	#ignore none signature proteins!

	@ECArr = split(/\|/,$ECs);
	@IPArr = split(/\|/,$IPs);

	$NumDo{$_}++ foreach (@IPArr);
	
	next if($ECs =~ /9\.9\.9\.9/);	#ignore none enzymes
	$NumEC{$_}++ foreach (@ECArr);

	foreach $ec (@ECArr){
		$Npair{$ec."\t".$_}++ foreach(@IPArr);
	}

};
close(FILE);

#======================================
#Calc raw association:
%RawAS = ();
%MaxEC = ();
%MaxIP = ();
foreach $pair (keys %Npair){
	($ec,$ip) = split(/\t/,$pair); 
	$RawAS{$pair} = 2* $Npair{$pair} / ($NumEC{$ec} + $NumDo{$ip});

	#Max ass score for each ec:
	if (!exists $MaxEC{$ec}) { 
		$MaxEC{$ec} = $RawAS{$pair};
	}else{
		$MaxEC{$ec} = max ($MaxEC{$ec}, $RawAS{$pair}); 	
	};
	
	#Max ass score for each ip:
	if (!exists $MaxIP{$ip}) { 
		$MaxIP{$ip} = $RawAS{$pair};
	}else{
		$MaxIP{$ip} = max ($MaxIP{$ip}, $RawAS{$pair}); 	
	};
};#each association pair


#======================================
#Calc normalized association score:
%NorAS = ();
foreach $pair (keys %RawAS){
	($ec,$ip) = split(/\t/,$pair);
	$NorAS{$pair} = 2 * $RawAS {$pair} / ( $MaxEC{$ec} + $MaxIP{$ip} );
};


#======================================
#Output:

#raw:
$tmpFile = "$rawFile.tmp";
open(FILE,">",$tmpFile);
#print FILE $_."\t". $RawAS{$_} ."\n" foreach (keys %RawAS);
foreach $pair (keys %RawAS){
	($ec,$pf) = split(/\t/,$pair);
	print FILE $pair."\t". sprintf("%.15f",$RawAS{$pair}) ."\t". $Npair{$pair} ."\t". $NumEC{$ec} ."\t". $NumDo{$pf}  ."\n";	
};
close(FILE);

system("sort -k1,1 -k3,3nr $tmpFile > $rawFile");
unlink($tmpFile);
print "Writing file [$rawFile]: DONE\n\n";

#normolized:
$tmpFile = "$norFile.tmp";
open(FILE,">",$tmpFile);
#print FILE $_."\t". $NorAS{$_} ."\n" foreach (keys %NorAS);
foreach $pair(keys %NorAS){
	($ec,$pf) = split(/\t/,$pair);
	print FILE $pair."\t". sprintf("%.15f",$NorAS{$pair}) ."\t". $Npair{$pair} ."\t". $NumEC{$ec} ."\t". $NumDo{$pf}  ."\n"
}
close(FILE);

system("sort -k1,1 -k3,3nr $tmpFile > $norFile");
unlink($tmpFile);
print "Writing file [$norFile]: DONE\n\n";

#######################################
#######################################

sub max{
	my $a = shift;
	my $b = shift;
	return $b if ($a < $b);
	return $a;
};










