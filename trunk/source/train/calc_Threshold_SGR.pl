#! /usr/bin/perl -w

use List::Util qw(max min);

# Using BEST DOMAIN score!

#INPUT:
$ec_inp			= $ARGV[0];	# example "ECm_95", or "1.2.1.2"
$hmmSearchOut 	= $ARGV[1];
$mem_sgr_File	= $ARGV[2];	# Idlist file for building msa, hmm, ...
$OutFile		= $ARGV[3];
#$sgr			= $ARGV[4];	# 1.1.1.1.sgr1
#$sgr = "no need";

#OUTPUT:
$OutFile		= $ARGV[3];

#$annFile = "../../data/spACID.ann";
$infoFile = "../../data/Enz.info";
$zero = 1e-15;

#=================================================
## get subgroup members:
open(FILE,$mem_sgr_File) || die;
%MemSgr = ();
while(<FILE>){
	chomp($_);
	($id) = split;
	$MemSgr{$id} = 1; #print $id."|\n"; 
}
close(FILE);

#=================================================
## get members of the ec:
open (FILE, $infoFile) || die "no [$infoFile] file\n";
%MemEC = ();
%ECsOfspACID = ();
%ECsOfEC = ();
#read the enzyme infor file
while(<FILE>){
	#($myEC,$spACID,$len,undef,undef,undef,$ECs) = split; 	# 9.9.9.9 sp|Q6GZV8|017L_FRG3G 502 654924 NONE NONE 9.9.9.9| AS:NONE DB:NONE MT:NONE CH:NONE
	($myEC,$spACID,$ECs) = split;
	$ECsOfspACID{$spACID} = $ECs;
	$ECsOfEC {$myEC} = $ECs;
}
close(FILE);

## check member for each spACID: 
$theSGR_ECs = $ECsOfEC{$ec_inp};	# $ECsOfEC{ECm_95} = 2.6.1.1|4.1.1.12|

foreach $spACID (keys %ECsOfspACID){	# e.g. ECm_98	Q8CPQ1	ATL_STAES	1335	3.5.1.28|3.2.1.96|	176280	PF01510|PF01832|
	$currECs = $ECsOfspACID{$spACID};	# $currECs = 3.5.1.28|3.2.1.96|

	if ($theSGR_ECs ne $currECs){		# the current ECs is not the same as the ECs of the SGR
		next if (not(is_Sub_Set($theSGR_ECs, $currECs) == 1));	# Check whether the 1st $theSGR_ECs is the sub-set of the 2nd $currECs
	};
	$MemEC{$spACID} = 1;
	#print "$my_spACID|\n";
}


#=================================================
#get list of hit IDs and bitscores:
%ScoreOfHitID = ();
@HitIDs = ();

@AllScore = ();
@MemSgrScore = ();
@MemECScore = ();
@MemNotECScore = ();

%FlagOfAScore = ();
$flag_memberOfTheSubgroup 	= "S";
$flag_memberOfTheEC 		= "E";
$flag_memberOfTheNotEC 		= "N";
$flag_GA 					= "g";
	
#read the search out file:
#========================
open(FILE,$hmmSearchOut)||die;
while(<FILE>){
	next if (/^>/);
	#($spACID,undef,$bscore) = split(/\s+/,$_); # full bscore
	($spACID, undef, undef, undef, undef, $bscore) = split;	# BEST DOMAIN score! 

	$ScoreOfHitID {$spACID} = $bscore;
	push(@HitIDs,$spACID);
	push(@AllScore, $bscore);

	if (exists $MemSgr{$spACID}){		# is a member of the subgroup	(true positive)
		push(@MemSgrScore, $bscore);
		if (exists $FlagOfAScore {$bscore}) {$FlagOfAScore {$bscore} = $FlagOfAScore {$bscore} . $flag_memberOfTheSubgroup;}
		else{$FlagOfAScore {$bscore} = $flag_memberOfTheSubgroup;};

	}else {
		if (exists $MemEC {$spACID}){	# is a member of the EC-set		(considered true positive)
			push(@MemECScore,$bscore);
			if (exists $FlagOfAScore {$bscore}) {$FlagOfAScore {$bscore} = $FlagOfAScore {$bscore} . $flag_memberOfTheEC;}
			else{$FlagOfAScore {$bscore} = $flag_memberOfTheEC;};
		} else{							# is a member of the not of EC-set	(false positive)
			push(@MemNotECScore,$bscore);
			if (exists $FlagOfAScore {$bscore}) {$FlagOfAScore {$bscore} = $FlagOfAScore {$bscore} . $flag_memberOfTheNotEC ;}
			else{$FlagOfAScore {$bscore} = $flag_memberOfTheNotEC ;};
		}
	}; # MemSgr ?
}
close(FILE); 
# end reading


if (! @MemNotECScore){
	$bscore = 0;
	push(@MemNotECScore,$bscore);
	if (exists $FlagOfAScore {$bscore}) {$FlagOfAScore {$bscore} = $FlagOfAScore {$bscore} . $flag_memberOfTheNotEC ;}
	else{$FlagOfAScore {$bscore} = $flag_memberOfTheNotEC ;};
}
if (! @MemECScore){
	$bscore = 0;
	push(@MemECScore,$bscore);
	if (exists $FlagOfAScore {$bscore}) {$FlagOfAScore {$bscore} = $FlagOfAScore {$bscore} . $flag_memberOfTheEC;}
	else{$FlagOfAScore {$bscore} = $flag_memberOfTheEC;};
}


#=====================
## Sorting... descreasing:
@MemSgrScore_sorted = sort {$b <=> $a} @MemSgrScore;
@MemNotECScore_sorted = sort {$b <=> $a} @MemNotECScore;

$TnoFN = $MemSgrScore_sorted [-1] + 0.0;	# min in the member;
$TnoFP = $MemNotECScore_sorted[0] + 0.05;	# max in the not-member;
if ($TnoFN > $TnoFP){
	$TGA = ($TnoFN + 2*($TnoFP-0.05))/3;
	push(@AllScore, $TGA);
	$FlagOfAScore {$TGA} = $flag_GA;
}

#Sorting all the scores: descreasing
@AllScore_sorted = sort {$b <=> $a} @AllScore;
$nscore = $#AllScore_sorted +1;
#=====================

######################################### Most important thing!!
######################################### Most important thing!!
######################################### Most important thing!!
$OptimumThreshold = ();
## Optimize the threshold:		
$MaxF1 = -1;
$MaxF1_arg = 0;
for($i = 0; $i <= $#AllScore_sorted; $i++){
	$testScore = $AllScore_sorted [$i];
	$curF1 = F1_for_the_Score($testScore);
	if ($MaxF1 <=  $curF1){
		$MaxF1 = $curF1;
		$MaxF1_arg = $i;
	}
} # for

if ($MaxF1_arg <$nscore-1){
	$OptimumThreshold = ($AllScore_sorted[$MaxF1_arg] + 1*$AllScore_sorted[$MaxF1_arg+1]) / 2;
} else {
	$OptimumThreshold = ($AllScore_sorted[$MaxF1_arg] + 0) /2;
}

# find next_noFN: max false positive that smaller than noFN
$Tnext_noFN = ();
foreach (@AllScore_sorted){
	$Tnext_noFN = $_;
	last if ($Tnext_noFN < $TnoFN);
};

$TGA = ($TnoFN + 2*$Tnext_noFN)/3;

##OUTPUT:
#===========================
# Output the threshold info:
open(FILE, ">$OutFile");

print FILE ">noFN=\t$TnoFN\t". myPrint($TnoFN)  ."\n";
print FILE ">noFP=\t$TnoFP\t". myPrint($TnoFP) ."\n";
print FILE ">GA=\t$TGA\t"    . myPrint($TGA) ."\n";
print FILE ">Optimal_threshold=\t$OptimumThreshold\t". myPrint($OptimumThreshold). "\n";


foreach $aScore (@AllScore_sorted){
	print FILE $aScore ."\t". UniqueIt( $FlagOfAScore{$aScore} ) ."\t". myPrint($aScore) ."\n"; # pre <t> rec <t> f1
}
close(FILE);
###END

#print $sgr ."\t". $OptimumThreshold  ."\t". myPrint($OptimumThreshold). "\n";
#print $sgr ."\t". $TGA  ."\t". myPrint($TGA). "\n";
#print $sgr ."\t". $TGA  ."\t". myPrint($TGA) ."\t". $OptimumThreshold  ."\t". myPrint($OptimumThreshold). "\n";

#=======================
#=======================
sub myPrint{
	my $aScore = shift;
	my ($TP,$FP,$FN,$TPex)	= countStat($aScore);
	my $Rec	= trunc(100* recall($TP,$FP,$FN));
	my $Pre	= trunc(100* precision($TP,$FP,$FN));
	my $F1		= trunc(1* F1byPreAndRec ($Pre, $Rec));

	my $PreEx	= trunc(100* precision($TPex,$FP,$FN));

	my $str = $PreEx ."\t". $Pre ."\t". $Rec ."\t". $F1 ;#."\t". $expectedCorrectness ;
	return $str;
}

sub precision{
	my $myTP = shift;
	my $myFP = shift;
	my $myFN = shift;	
	my $Pre	=	$myTP / ($myTP + $myFP + $zero);	
	return $Pre;
}

sub recall {
	my $myTP = shift;
	my $myFP = shift;
	my $myFN = shift;	
	my $Rec	=	$myTP / ($myTP + $myFN + $zero);
	return $Rec;
}

sub F1byPreAndRec{
	my $Pre = shift;
	my $Rec = shift;
	my $F1 	= 2* $Pre * $Rec / ($Pre + $Rec + $zero);
	return $F1;
}

sub F1measure{
	my $myTP = shift;
	my $myFP = shift;
	my $myFN = shift;	
	my $Pre	=	$myTP / ($myTP + $myFP + $zero);
	my $Rec	=	$myTP / ($myTP + $myFN + $zero);	
	my $F1 	= 2* $Pre * $Rec / ($Pre + $Rec + $zero);
	return $F1;
}


sub countStat{
	my $my_bscore = shift;
	my $myTP = 0.0;
	my $myFP = 0;
	my $myFN = 0;

	foreach $sc (@MemSgrScore){
		$myTP +=1 if ($sc >= $my_bscore);
		$myFN ++ if ($sc <  $my_bscore);		
	};
	foreach $sc (@MemNotECScore){
		$myFP ++ if ($sc >= $my_bscore);	
	};

# adjust TP count for smoothness
	my $myTPex = $myTP;
	$myTPex = 5 if ($myTP <5);	# raise up for small count
	$myTPex = 10.0 + $myTP/2 if ($myTP > 20); # slow down for high count

	return $myTP, $myFP, $myFN, $myTPex;
}


sub F1_for_the_Score{
	my $my_bscore = shift;
	my ($myTP, $myFP, $myFN,undef) = countStat($my_bscore);
	my $myF1 = F1measure ($myTP, $myFP, $myFN);
	return  $myF1;
};

sub trunc{
	my $data = shift;
	return sprintf("%.2f",$data);
}

sub UniqueIt{
	my $str = shift;
	my %Set = ();
	for($i = 0; $i <length($str); $i++){
		$c = substr($str,$i,1);
		$Set {$c}++;
	}
	my $myStr = "";
	$myStr = $myStr . $_ for (keys %Set);
	return $myStr;
}

sub is_Sub_Set{	# check whether (1st) $theECs is a sub-set of (2nd) $currECs:
	my $theECs	= shift;	# 1.2.9.-|3.3.6.4|
	my $currECs	= shift;	# 1.2.9.5|3.3.6.44|

	my @TMPArr = split(/\|/,$theECs);

	foreach $ec (@TMPArr){
		$ec =~ s/\Q.-\E//g;
#		print "test for $ec:\n";
		$match = 0;
		$match ++ if ($currECs =~ m/\Q$ec.\E/);
		$match ++ if ($currECs =~ m/\Q$ec|\E/);
		return 0 if ($match < 1);
#		print "test for $ec: GOOD!\n";;
	};

	return 1;
}

#test: 
#$u = "1.2.-.-|1.2.9.33|";
#$v = "1.2.9.33|1.2.9.3";
#if (is_Sub_Set($u, $v) == 1){
	#print "$u is sub a set of $v\n" ;
#}else{
#print "$u is NOT a sub set of $v\n";};



