#! /usr/bin/perl -w
#==============================================================================
# This scripts generates all subgroups' information:
#
# List of subgroups is stored at "data/SGR.list"
#
# Subgroup data is stored at "SGRs/" folder
#
# Subgroup data:
# File ecid.sgrXXX.Idlist 	:	list of spACIDs for subgroup XXX of the EC ecid
# e.g.: ECm_780.sgr2.Idlist
#
#File ecid.sgrXXX.fasta 	:	fasta file for subgroup XXX of the EC ecid
# e.g.: ECm_780.sgr2.fasta
#==============================================================================


$MAX_LEN_ALLOW = 4000;

#===INPUT===#
$DataDir 	= "../../data";
$annFile = "$DataDir/spACID.ann";
$fastaFile 	= "../../Swiss-prot/SwissProt.fasta";
$ecSgrPatFile = "$DataDir/EC-sgr-patterns.tab";

#===OUTPUT===#
$OutDir 	= "../../SGRs";
system("mkdir -p $OutDir");
$sgrListFile 	= "$DataDir/SGR.list";	##!!

#$sgrInfoFile = "$OutDir/sgrXX.info";
#$sgrIdFile = "$OutDir/sgrXX.Idlist";
#$sgrFastaFile = "$OutDir/sgr0.fasta";

#=== Global vars===#
%FastaOfID = ();
%spACIDOfECPFs = ();
%LenOf = ();
%TaxOf = ();
%SitesOfspACID = ();
$startTime = time;

#========================================
# Hashing uniprot fasta file:
print "Reading uniprot fasta[$fastaFile]:\n";
#>sp|B8Y466|SRPK3_PIG 566 9823 PF00069| 2.7.11.1|
#MSASTGGGGGGDSGSSSSSSSQASCGPEPSGSELAPPTPAPRMLQGLLGSDDEEQEDPKD
open(FILE,$fastaFile) || die "Error: cannot open $fastaFile file.\n";

while ($line = <FILE>){
	next if (length($line) <1);
	if ($line =~ /^>/) {	# header line
		($header) = split(/\s+/, $line);
		$myspACID = substr($header,1);		# sp|P51124|GRAM_HUMAN
		$FastaOfID{$myspACID} = $header."\n";
		next;		
	}
	$FastaOfID{$myspACID} = $FastaOfID{$myspACID} . $line;		
}
close(FILE);
print "Reading fasta file [$fastaFile]:	DONE.\n\n";


#========================================
# Read spACID annotation file:
#ECm_315 sp|Q9UBQ7|GRHPR_HUMAN 328 9606 PF00389|PF02826| IPR006139|IPR006140|IPR016040| 1.1.1.79|1.1.1.81| AS:293:H| DB:217:S|243:I|245:R|269:D|295:G| MT:NONE CH:NONE
print "Reading spACID annotation file [$annFile]:\t";
open(FILE, $annFile) ||die;
while(<FILE>){
	chomp;
	($ec,$spACID,$len, $tax, $PFs, undef, undef, $AS, $BD, $MT, $CH) = split;
	next if ($ec =~ /9.9.9.9/);	# excluding the non-enzymes	
	
	(undef, $len) = split(/=/,$len);

	$LenOf {$spACID} = $len;
	$TaxOf {$spACID} = $tax;

	$SitesOfspACID{$spACID} = $AS." ".$BD." ".$MT." ".$CH;

	$PFs = "0" if (not $PFs);

	if (exists $spACIDOfECPFs {$ec ."\t". $PFs}){
		$spACIDOfECPFs {$ec ."\t". $PFs} = $spACIDOfECPFs {$ec ."\t". $PFs} . $spACID ."\n";
	}
	else {$spACIDOfECPFs {$ec ."\t". $PFs} = $spACID."\n";}
}
close(FILE);
print "DONE\n\n";


#========================================
# Read EC subgroup domain pattern file:
#>2.3.1.101	1
#2.3.1.101	1	PF01913|PF02741|
#>ECm_526	3
#ECm_526	1	PF00270|PF00271|PF00680|PF00767|PF00863|PF08440|
print "Reading EC subgroup pattern file [$ecSgrPatFile]: ";
%LinesOfEC = ();
open(FILE,$ecSgrPatFile)||die;
while(<FILE>){
	if (/^>/){							#>ECm_526	3
		($ec) = split(/\s+/,$_);
		$ec = substr($ec,1);			# trim the >
		$LinesOfEC{$ec} = "";
		next;
	}
	$LinesOfEC{$ec} = $LinesOfEC{$ec} . $_;
}
close(FILE);
print "DONE\n\n";


#===================================================
# Main thing to do:
print "\nGet subgroups for each EC:\n";
#test:
#getSgrForAnEC("1.1.1.1");

open(SGRFILE,">",$sgrListFile);
$cnt = 0;
foreach (keys %LinesOfEC){
	$cnt++;
	getSgrForAnEC($_);
	#last if ($cnt > 10);
}
close(SGRFILE);
print "DONE everything!\n";
print "Running time: ". (time - $startTime) ." [seconds]\n\n";

### END

####################################################
####################################################
sub getSgrForAnEC{
	my $ec = shift;
	my @Lines = split(/\n/,$LinesOfEC{$ec});
	local (%PFsOfSgr, @PFsArr, $PFs, @IDlist, $IdlistFile, $fastaFile, $sgr );#, $infoFile);

	%PFsOfSgr = ();

	foreach (@Lines){ #ECm_526	1	PF00270|PF00271|PF00680|PF00767|PF00863|PF08440|
		chomp;
		(undef, $sgr, $PFs) = split;
		if (exists $PFsOfSgr{$sgr} ){
			$PFsOfSgr{$sgr} = $PFsOfSgr{$sgr} ."\t". $PFs;
		}else{
			$PFsOfSgr{$sgr} = $PFs;
		}
	};


	foreach $sgr(keys %PFsOfSgr){
		@PFsArr = split(/\t/,$PFsOfSgr{$sgr} );
		@IDlist = ();
		foreach $PFs(@PFsArr){
			my @tmpArr = split(/\n/,$spACIDOfECPFs{$ec ."\t". $PFs});
			push(@IDlist,@tmpArr);
		};

		#print the Id list file:
		$IdlistFile = $OutDir ."/$ec.sgr$sgr.Idlist";
		open(MYFILE, ">", $IdlistFile);	
		for(@IDlist){
			print MYFILE $_. "\t". $SitesOfspACID{$_}."\n";		
		}
		close(MYFILE);

		#print the fasta file:
		$fastaFile = $OutDir ."/$ec.sgr$sgr.fasta";
		open(MYFILE, ">", $fastaFile);	
		for (@IDlist){
			print MYFILE $FastaOfID{$_} if ($LenOf{$_} < $MAX_LEN_ALLOW);
		}
		close(MYFILE);
		print SGRFILE $ec .".sgr$sgr\n";		
	}
}
