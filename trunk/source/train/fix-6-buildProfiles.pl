#! perl -w
#==============================================================================
# Non-threading version
#==============================================================================

#===INPUT===#
$DataDir 	= "../../data";
$SGRlistFile = "$DataDir/SGR.list";
$SGRDir = "../../SGRs";
$uniprotFasta 	=  "../../Swiss-prot/SwissProt.fasta";

# Dependent scripts:
$hmmBuild_Script 		= "build_MSA_HMM.pl";
$hmmSearch_Script		= "searchHMM.pl";
$calcThreshold_Script	= "calc_Threshold_SGR.pl";
$findSite_Script		= "FindSites.pl";

#===OUTPUT===#
$msaDir = "../../MSAs";
system("mkdir -p $msaDir");
system("mkdir -p ../log");
$hmmDir = "../../HMMs";
system("mkdir -p $hmmDir");

#=======================================
# Read the list of SGRs:
#4.2.3.19.sgr1
#ECm_332.sgr1
%SGRSet = ();
open(FILE, $SGRlistFile) || die "no [$SGRlistFile] file\n";
while(<FILE>){
	chomp($_);	
	($sgr) = split(/\s+/,$_);
	$SGRSet{$sgr}++;	
}
close(FILE);
@SGRList = ();
push(@SGRList,$_) for(keys %SGRSet);

#=======================================
# Run for each SGR:
$total = scalar keys %SGRSet;
$n = 0;
$startTime = time;
foreach $sgr (@SGRList){	
	$n++;
#	next if !($sgr =~/\Q1.1.1.\E/);
	next if ($sgr !~ /\.-\.-/); 	### FIX here!!!

	$elapsed = time - $startTime;
	print "\n";
	system ("date");
	print "[$n / $total] Running for $sgr: [$elapsed seconds]                         \n"; # if ($n % 10 == 0);

	#next if ($n<100);
	#last if ($n>120);


	$fastaFile  = $SGRDir 		. "/$sgr.fasta";
	$msaFile	= $msaDir 	. "/$sgr.msa";
	$hmmFile	= $hmmDir 	. "/$sgr.hmm";

	#build MSA and HMM for this fasta file:
	print "\tBuilding MSA and HMM:\n";
	system("perl $hmmBuild_Script $sgr $fastaFile $msaFile $hmmFile");
	print "\tBuilding MSA and HMM: DONE!\n";

	
	#search HMM against uniprot SP:
	print "\tSearch against uniprot:\n";
	$hmmSearchOut 		= $hmmDir . "/$sgr.hmm.search_out.sp";
	system("perl $hmmSearch_Script $hmmFile $uniprotFasta $hmmSearchOut");
	print "\tSearch against uniprot: DONE!\n";


	#calc threshold for profile:
	print "\tCalculate optimal cut-off threshold:\n";
	$IdlistFile 	= $SGRDir 		. "/$sgr.Idlist";
	$thresholdFile 	= $hmmDir . "/$sgr.hmm.threshold";
	($ec_inp) = split(/\.sgr/,$sgr);
	system("perl $calcThreshold_Script $ec_inp $hmmSearchOut $IdlistFile $thresholdFile $sgr");# >> $HMMthresholdsFile");
	print "\tCalculate optimal cut-off threshold: DONE!\n";

	#Find active sites and any other specific sites:
	print "\tFinding known specific sites:\n";
	$acitveSiteFile 	= $hmmDir . "/$sgr.hmm.sites";
	system("perl $findSite_Script $hmmSearchOut.raw $acitveSiteFile");
	print "\tFinding known specific sites: DONE!\n";
}

print "\n";
system ("date");

print "Total running time: ". (time - $startTime). " seconds\n";

### END

