#! /usr/bin/perl -w

$InFile = $ARGV[0];
open(FILE,$InFile) || die "Error $InFile reading.\n";
@LINES = <FILE>;
close(FILE);

# write msa file:
print "# STOCKHOLM 1.0\n\n";

($ID) 	= split(/\s+/,$LINES[0]);
$ID  	= substr($ID,1);
#$ID = substr($LINES[0],1,index($LINES[0]," "));
#$DE = substr($LINES[0],index($LINES[0]," "));
#print "#=GS\t$ID\tDE\t$DE\n";

for $i (1..($#LINES) ){
	print $ID . "\t" . $LINES[$i] . "\n";
}
print "//\n";

