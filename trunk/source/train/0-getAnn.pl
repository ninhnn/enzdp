#! /usr/bin/perl -w
#==============================================================================
# This scripts mines Swiss-prot database for sequence annotation.
# The Uniprot entry format can be found here: http://web.expasy.org/docs/userman.html
#
# The output format of annotation: 
#$ECID." ". $spACID ." ". $outLen ." ". $outTax ." ". $outPFs ." ". $outIPs ." ". $outECs ." ". $outAS ." ". $outBD ." ". $outMT ." ". $outCH ."\n";
# e.g.:
#ECm_315 sp|Q9UBQ7|GRHPR_HUMAN Len=328 Tax=9606 PF00389|PF02826| IPR006139|IPR006140|IPR016040| 1.1.1.79|1.1.1.81| AS=293:H| DB=217:S|243:I|245:R|269:D|295:G| MT=NONE CH=NONE
#
# The output EC list:
# File "EC.list" : List of EC numbers  <EC-id tab ECs|>
#6.6.1.2	6.6.1.2|
#ECm_1	2.1.1.-|2.1.1.-|2.7.7.48|3.6.4.13|3.6.4.13|
#
# The output fasta file:
#>sp|B8Y466|SRPK3_PIG Len=566 Tax=9823 PF00069| 2.7.11.1|
#MSASTGGGGGGDSGSSSSSSSQASCGPEPSGSELAPPTPAPRMLQGLLGSDDEEQEDPKD
#YDFG
#
#==============================================================================


#===INPUT===#
$DB = "../../Swiss-prot/uniprot_sprot.dat"; # input uniprot .dat file 

#===OUTPUT===#
$OutDir 	= "../../data";
system("mkdir -p $OutDir");
$OutFile 	= "$OutDir/spACID.ann";	##!!
$ecListFile = "$OutDir/EC.list";	##!!

$fastaFile 	= "../../Swiss-prot/SwissProt.fasta";
$siteFile	= "$OutDir/spACID.sites";

#=== Global vars===#
$multiEnzymeECCnt = 0;
%ECIDOfEC = ();
%FastaOf = ();
%SitesOf	= ();

#=================================================
print "==Processing Uniprot/Swiss-prot database:==\n\n";

###Reading database:
print "Reading file [$DB]:\n";
open(DATA,$DB) || die " No data file?? Exit.\n";
open(OUT,">",$OutFile);

###Uniprot entry format: http://web.expasy.org/docs/userman.html

while(<DATA>){
	chomp;
	#check ID lines:
	if (/^ID/){
		(undef, $myID, undef, $myLen) = split;
		$myAC = "";
		$ncbi_taxid = "";
		@ECs = ();
		@PFs = ();
		@IPs = ();

		@ASPos = ();	#active sites
		@BDPos = ();	#binding sites
		@MTPos = ();	#metal binding sites
		@CHPos = ();	#CARBOHYD: Glycosylation sites

		$seqFlag = 0;
		$FastaSeq = "";
		$FastaSeqStandart = "";
	};

	#check AC lines:
	if (/^AC/){
		(undef,$tmp) = split;
		($tmp,undef) = split(/;/,$tmp);

		if ($myAC eq ""){		
			$myAC = $tmp;
		}
	};

  	#check OX
	if (/^OX/){
		if (/NCBI_TaxID=/){
			(undef,$ncbi_taxid) = split(/NCBI_TaxID=|;/,$_);
			($ncbi_taxid) =  split(/\s+/,$ncbi_taxid);
		};
	};

  	#check DE
	if (/^DE/){
		if (/EC=/){
			(undef,$ec) = split(/EC=|;/,$_);
			($ec) = split(/\s+/,$ec);
			push(@ECs,$ec);
		};
	};

	#check DR lines (cross reference data):
	if (/^DR/){
		#Pfam:
		if (/^DR\s+Pfam/){
			(undef,$myPFamID) = split(/;\s+/);
			push(@PFs,$myPFamID);
		};

		#InterPro:
		if (/^DR\s+InterPro/){
			(undef,$myInterPro) = split(/;\s+/);
			push(@IPs,$myInterPro);
		};
	};#

	#check FT lines (features)
	if (/^FT/){
		#ACT_SITE: active site: Amino acid(s) involved in the activity of an enzyme
		if (/^FT\s+ACT_SITE/){
			(undef,undef,$position) = split;
			push(@ASPos,$position);
		}
		#BINDING: binding site: Binding site for any chemical group (co-enzyme, prosthetic group, etc.)
		if (/^FT\s+BINDING/){
			(undef,undef,$position) = split;
			push(@BDPos,$position);
		}
		#METAL: Binding site for a metal ion
		if (/^FT\s+METAL/){
			(undef,undef,$position) = split;
			push(@MTPos,$position);
		};
		#CARBOHYD: Glycosylation site
		if (/^FT\s+CARBOHYD/){
			(undef,undef,$position) = split;
			push(@CHPos,$position);
		};
	};# FT features

	#check SeQuence line:
	if (/^SQ/){
		$seqFlag = 1;
	}
	#get sequence:
	if ((/^\s+/) and ($seqFlag ==1 )){
		$sq = $_;
		$sq =~ s/\s+//g;
		$FastaSeq = $FastaSeq . $sq;
		$FastaSeqStandart = $FastaSeqStandart . $sq ."\n";
	};


	#check // line (End of entry)
	if (/^\/\//){
		$spACID = "sp|$myAC|$myID";
		$outLen = "Len=$myLen";
		$outTax = "Tax=$ncbi_taxid";
		$outPFs = standardize(@PFs);
		$outIPs = standardize(@IPs);
		@ECs 	= checkECs(@ECs);
		$outECs = standardize(@ECs);
		
		#calc amino acid position:
		$outAS 	= FindSites(@ASPos);
		$outAS	= "AS=".$outAS;
		$outBD	= FindSites(@BDPos);
		$outBD	= "DB=".$outBD;
		$outMT	= FindSites(@MTPos);
		$outMT	= "MT=".$outMT;
		$outCH	= FindSites(@CHPos);		
		$outCH	= "CH=".$outCH;
		
		#calc EC-id:
		$ECID = setIdOfEC($outECs);

		print OUT $ECID ." ". $spACID ." ". $outLen ." ". $outTax ." ". $outPFs ." ". $outIPs ." ". $outECs ." ". $outAS ." ". $outBD ." ". $outMT ." ". $outCH ."\n";

		$header = $spACID ." ". $outLen ." ". $outTax ." ". $outPFs ." ".$outECs."\n";
		$FastaOf{$spACID} = ">".$header.$FastaSeqStandart;
		$SitesOf{$spACID} =$ECID ."\t". $spACID ."\t". $outAS ." ". $outBD ." ". $outMT ." ". $outCH. "\t".$outECs."\n" if (haveSite($outAS.$outBD.$outMT.$outCH) == 1);		
	};# End of entry

}; ### done reading swiss-prot
close(DATA);
close(OUT);
print "Reading file [$DB]:  DONE\n";
#=================================================
### Write known sites file:
print "Writing file [$siteFile]: \n";
open(OUT,">$siteFile");
print OUT $SitesOf{$_} for(keys %SitesOf);
close(OUT);
print "Writing file [$siteFile]: DONE.\n\n";


#=================================================
### Write output ec list: ECId \t ec
open(TMP,">$ecListFile.tmp");
print TMP $ECIDOfEC{$_} . "\t". $_ ."\n" for(keys %ECIDOfEC);
close(TMP);

print "Writing file [$ecListFile]: "; 
system("sort -k1,1  $ecListFile.tmp | grep -v \"9.9.9.9\" > $ecListFile");	
unlink("$ecListFile.tmp");
print "DONE.\n";

#=================================================
### Write fasta file:
print "Writing file [$fastaFile]: \n";
open(OUT,">$fastaFile");
print OUT $FastaOf{$_} for(keys %FastaOf);
close(OUT);
print "Writing file [$fastaFile]:	DONE.\n";

#######################################
#### Sub-routine ###
#######################################

sub haveSite{
	my $str = shift;
	my $cnt = $str =~ s/NONE/NULL/g;
	return 0 if ($cnt ==4);
	return 1;
};# have site


#============
sub checkECs{
	local (@myECs, %BadEC, $ec, $ec1, $ec2, $tmp2, @Res);

	@myECs = @_;
	if (! @myECs){
		push(@myECs,"9.9.9.9");
	};

	%ECSet = ();
	$ECSet{$_}++ foreach(@myECs);
	@myECs = ();
	push(@myECs,$_) foreach(keys %ECSet);

	%BadEC = ();
	foreach $ec (@myECs){
		$BadEC{$ec} = 0;
	}
	foreach $ec1 (@myECs){
		foreach $ec2 (@myECs){
			next if ($ec1 eq $ec2);
			next if ($ec2 !~ /-$/);
			$tmp2 = $ec2;
			$tmp2 =~ s/.-//g;
			$BadEC{$ec2} = 1 if (index("$ec1.","$tmp2.")==0);
		}
	}
	@Res = ();
	foreach $ec (@myECs){
		push(@Res,$ec) if ($BadEC{$ec} == 0);
	}
	
	return @Res;
};



#==================
sub standardize{
	local (@Arr, @sortedArr, $res);
	@Arr = @_;

	if (! @Arr){
		return "NONE";
	};

	%ArrSet = ();
	$ArrSet{$_}++ foreach(@Arr);
	@Arr = ();
	push(@Arr,$_) foreach(keys %ArrSet);


	@sortedArr = sort @Arr;		#sort string
	$res = "";
	foreach (@sortedArr){
		$res = $res . $_ ."|";
	};
	return $res;
};

#============
sub FindSites{
	local (@Pos, $Seq, $res );
	@Pos = @_;
	$Seq = $FastaSeq;
	if (! @Pos){
		return "NONE";
	};
	@Pos = sort { $a <=> $b } @Pos;		# sort numerically ascending
	$res = "";
	foreach $pos(@Pos){
		$char = substr($Seq,($pos-1),1);
		$res = $res . $pos .":".$char ."|";
	};

	return $res;
};


##################################################
sub setIdOfEC{
	my $ECs = shift;
	my $nECs = () = $ECs =~/\|/g;
	if ($nECs >1){		#multi enzymes
		if (not(exists $ECIDOfEC{$ECs})){
			$multiEnzymeECCnt++;
			$ECIDOfEC{$ECs} = "ECm_$multiEnzymeECCnt";
		}
	}else {		#mono enzymes:
		my $ec = $ECs;
		$ec =~ s/\|//g;
		$ECIDOfEC{$ECs} = $ec;
	}
	return $ECIDOfEC{$ECs};
}
