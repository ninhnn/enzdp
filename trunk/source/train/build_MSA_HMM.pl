#! /usr/bin/perl -w

### Input: An EC number and a fasta file
$sgr = $ARGV[0];
$fastaFile = $ARGV[1];

### Output: A MSA file and an HMM file
$msaFile = $ARGV[2];
$hmmFile = $ARGV[3];
### external progs:
$clustalo 		= "../../progs/clustalo";
$OutFormat 		= "st"; 	# Stockholm
$hmmbuild 		= "../../progs/hmmbuild";
$ScriptFor1Seq	= "perl createMSAfor1Seq.pl";

### Run it, LONG TIME to wait...


#system("date");
print "\t\tRUN clustalo:\n";
#Run clustalo:	
$seqcnt = `grep '>' $fastaFile | wc -l`;
if ($seqcnt ==1){	# build MSA for 1 sequence
	system("$ScriptFor1Seq $fastaFile > $msaFile"); 
	system("echo \"One seq! for $fastaFile\" | cat - >> ../log/$sgr.clustal.log");}
else{				# build MSA for multiple sequences
	system("echo \"$seqcnt for $fastaFile : \" | cat - >> ../log/$sgr.clustal.log");
	system("$clustalo -i $fastaFile -o $msaFile --outfmt $OutFormat --thread=4 --force -v >>../log/$sgr.clustal.log");
}
print "\t\tDONE MSA\n";
#system("date");

#Run hmmbuild:	
#print "\t\tbuild hmm for $hmmFile and $msaFile: \n";	
system ("$hmmbuild $hmmFile $msaFile >> ../log/$sgr.hmmbuild.log");
print "\t\tDONE HMM building\n";
#system("date");

