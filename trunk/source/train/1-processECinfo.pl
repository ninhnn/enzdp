#! /usr/bin/perl -w
#==============================================================================
# This scripts creates several files for EC numbers:
#
#
# File "EC.domains.tab": List of domain patterns for each EC-id
#$pair ."\t".	$numPFs	."\t". $ECDomPair{$pair} ."\t".  $aver ."\t". $stdev ."\t". $MAD ."\n";
#
# File "EC.patterns.tab": List of domain patterns for each EC-id, with number of patterns calculated
#$ec ."\t". $NPats{$ec} ."\t". $PFs  ."\t". $numPFs	."\t". $ECDomPair{$pair} ."\t".  $aver ."\t". $stdev ."\t". $MAD ."\n";
#
# File "EC.isolated.tab": Information about whether a pattern is isolated to an EC or not.
#$ec ."\t". $NPats{$ec} ."\t". $PFs  ."\t". $nSeqs ."\t".  $nOtherSeqs ."\t". $otherList ."\n";
#
#==============================================================================



#===INPUT===#
$DataDir = "../../data";
$annFile = "$DataDir/spACID.ann";

#===OUTPUT===#


$mytmpfile = "tmp.tmp";
$ECdomFile = "$DataDir/EC.domains.tab";
$ECpatFile = "$DataDir/EC.patterns.tab";
$ECisolated = "$DataDir/EC.isolated.tab";
$EnzInfoFile="$DataDir/Enz.info";

#=== Global vars===#
=comment
$multiEnzymeECCnt = 0;
%ECIDOfEC = ();
=cut

%ECDomPair = ();
%LenOfPair = ();

#=================================================
### Read data file:
open(FILE,$annFile) || die "ann file missing?\n";
open(OUT,">",$EnzInfoFile);
print "Reading file [$annFile]: \n"; 
while(<FILE>){	#ECm_315 sp|Q6GZX3|002L_FRG3G L320 654924 PF03003| IPR004251| 9.9.9.9| AS:NONE DB:NONE MT:NONE CH:NONE
	chomp;
	($ec, $spACID,$seqLen,undef,$PFs,undef, $ecraw) = split;
	(undef,$seqLen) = split(/=/,$seqLen);

	$ECDomPair {$ec ."\t". $PFs}++;
	if (exists $LenOfPair {$ec ."\t". $PFs} ){
		$LenOfPair {$ec ."\t". $PFs} = $LenOfPair {$ec ."\t". $PFs} . $seqLen."\t";
	}
	else { $LenOfPair {$ec ."\t". $PFs} = $seqLen."\t";}

	print OUT $ec ."\t". $spACID ."\t". $ecraw ."\n" if ($ec !~ /\Q9.9.9.9\E/);

}
close(FILE);
print "Reading file [$annFile]: DONE\n\n"; 
close(OUT);
print "Writing file [$EnzInfoFile]: DONE\n\n"; 
#=================================================
# Write the EC.domains.tab
open(TMP,">$mytmpfile");
foreach $pair (keys %ECDomPair){
	$numPFs=()= ($pair =~ /\|/g);
	($aver,$stdev, $MAD) = calcAVERAndSTDEV($LenOfPair{$pair});
	print TMP $pair ."\t".	$numPFs	."\t". $ECDomPair{$pair} ."\t".  $aver ."\t". $stdev ."\t". $MAD ."\n";
}
close(TMP);


print "Writing file [$ECdomFile]: "; 
system("sort -k1,1 -k3,3nr -k2,2 $mytmpfile > $ECdomFile");	# sort by $ec then by $myID (gene_name)
unlink("tmp.tmp");
print "DONE.\n\n";

#=================================================
# Write #patterns for each EC: EC.patterns.tab
%NPats = ();
foreach $pair (keys %ECDomPair){
	($ec,undef) = split(/\t/,$pair);
	$NPats{$ec} ++;
};


open(TMP,">$mytmpfile");
foreach $pair (keys %ECDomPair){
	$numPFs=()= ($pair =~ /\|/g);
	($aver,$stdev, $MAD) = calcAVERAndSTDEV($LenOfPair{$pair});
	($ec,$PFs) = split(/\t/,$pair);
	print TMP $ec ."\t". $NPats{$ec} ."\t". $PFs  ."\t". $numPFs	."\t". $ECDomPair{$pair} ."\t".  $aver ."\t". $stdev ."\t". $MAD ."\n";
}
close(TMP);


print "Writing file [$ECpatFile]: "; 
system("sort -k2,2n -k1,1 -k4,4nr $mytmpfile > $ECpatFile");	
unlink("tmp.tmp");
print "DONE.\n\n";

#==================================================
# Find isolated enzymes EC.isolated.tab
%ECandnSeqsOfPF = ();
foreach $pair (keys %ECDomPair){
	($ec,$PFs) = split(/\t/,$pair);
	$nSeqs = $ECDomPair{$pair};
	if (exists $ECandnSeqsOfPF {$PFs } ){
		$ECandnSeqsOfPF {$PFs } = $ECandnSeqsOfPF {$PFs } . "$ec|$nSeqs" ."\t";	
	} else { $ECandnSeqsOfPF {$PFs } = "$ec|$nSeqs" ."\t";};
}


open(TMP,">$mytmpfile");
foreach $pair (keys %ECDomPair){
	($ec,$PFs) = split(/\t/,$pair);
	$nSeqs = $ECDomPair{$pair};
	($nOtherSeqs, $otherList) = countOtherSeqs($ec, $ECandnSeqsOfPF {$PFs } );	
	print TMP $ec ."\t". $NPats{$ec} ."\t". $PFs  ."\t". $nSeqs ."\t".  $nOtherSeqs;
	print TMP "\t". $otherList if (!($PFs =~ /NONE/));
	print TMP "\n";
}
close(TMP);

#$ECisolated = "$OutDir/EC.isolated.3pats.zero.tab";

print "Writing file [$ECisolated]: "; 
system("sort -k2,2n -k1,1 -k4,4nr $mytmpfile > $ECisolated");	
unlink("tmp.tmp");
print "DONE.\n\n";


##################################################
##################################################
##################################################


#($nOtherSeqs, $OtherList) = countOtherSeqs($ec, $ECandnSeqsOfPF {$PFs } );
sub countOtherSeqs{	
	local ($ec, $allLists, $nOtherSeqs, $otherList);
	$ec = shift;
	$allLists = shift;
	my @LISTs = split(/\t/,$allLists);		# "$ec|$nSeq)" \t
	$nOtherSeqs = 0;
	$otherList = "";
	foreach my $ec_nSeqs (@LISTs){
		($myEC,$mynSeqs) = split (/\|/,$ec_nSeqs);
		next if ($myEC eq $ec);
		$nOtherSeqs += ($mynSeqs + 0.0);
		$otherList = $otherList. "($myEC|$mynSeqs)" ."\t"; 
	}	
	return ($nOtherSeqs, $otherList);
}

#===========
sub sortIDs{
	my $str = shift;
	my @IDs = split(/\|/,$str);
	@IDs_sorted = sort @IDs;
	my $res = "";
	foreach (@IDs_sorted){
		$res = $res . $_ ."|";
	};
	return $res;
}

#========================
sub average{	#$ave = &average(\@LenArr);
        my($data) = @_;
        if (not @$data) {
                #die("Empty array\n");
        }
        my $total = 0;
        foreach (@$data) {
                $total += $_;
        }
        my $average = $total / @$data;
        return $average;
}

sub stdev{	#$std = &stdev(\@LenArr);
        my($data) = @_;
        if(@$data == 1){
                return 0;
        }
        my $average = &average($data);
        my $sqtotal = 0;
        foreach(@$data) {
                $sqtotal += ($average-$_) ** 2;
        }
        my $std = ($sqtotal / (@$data-1)) ** 0.5;
        return $std;
}
	
sub calcAVERAndSTDEV {
	my $str = shift;
	my @LenArr = split(/\t/,$str);
	my $aver = &average(\@LenArr);
	my $stdev = &stdev(\@LenArr);
	my $nSeqs = $#LenArr +1;
	my $sum = 0;
	my $cnt = 0;
	for (my $i = 0; $i <$nSeqs; $i++){
		$sum += abs ($LenArr[$i] - $aver);
		$cnt ++;
	}
	my $MAD = 0;	# mean absolute deviation
	$MAD = $sum / $cnt  if ($cnt >0);

	return (trunc($aver), trunc($stdev), trunc($MAD));
}

sub trunc {
	$data = shift;
	return sprintf("%.4f",$data);
}
	
##### END #####
###############


