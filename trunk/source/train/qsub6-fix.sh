#submit to following queues only:
#$ -q compsma3.q,compsma5.q,compsma10.q,compsma11.q
#compsma35.q,compsma36.q,compsma37.q,compsma35.q,compsma36.q,compsma37.q,compsma38.q,compsma39.q,compsma40-express.q,compsma40.q,compsma41.q,compsma42.q,compsma43.q,compsma44.q,compsma45.q,compsma46.q,compsma47.q,compsma48.q,compsma49.q,compsma50.q

#move to current working directory, where the script is submitted.
#$ -cwd

#$ -l arch=lx24-amd64

# print output to the following folder:
#$ -o $HOME/qsub-out/

# join the .o. and .e. file
#$ -j y

# move to current working folder
cd $SGE_O_WORKDIR

echo "Current working dir:"
echo $SGE_O_WORKDIR
echo

echo "task id is $SGE_TASK_ID"
echo "host running this task is:"
hostname

free -m 
cat /proc/meminfo
echo
date

#perl 6-buildProfiles.pl

perl 6-buildProfiles-fix.pl

free -m 
cat /proc/meminfo
date
