===================================================
EnzDP - Enzyme annotation using domain HMM profiles
===================================================

Software Dependency:
--------------------
    + Perl 5.10+
    + Python 2.6/2.7
    + Linux 64bit
    EnzDP also uses clusto and HMMER 3, whose binaries are included in "progs/" 
    folder   

Data Dependency:
----------------
    EnzDP trains its profiles based on Uniprot/Swissprot database. Trained data 
    is provided in this release. Perl & Python scripts for training is also 
    available.

Installation:
-------------
    No installation is required. EnzDP can be run directly from command line, 
    provided that all dependencies are satisfied.

Steps to Run EnzDP:
-------------------
    1. Create a project file .py, specifying location of input and output.
    Example of project file "sample_proj.py":
    FASTA_FILE='/home/username/EnzDPWeb/EnzDP/TestDS/PF01048/PF01048.fasta'
    THRESHOLD='0.05'
    OUTPUT_FILE='/home/username/EnzDPWeb/EnzDP/TestDS/PF01048/PF01048.enzdp.res'

    Note: you are recommended to use a small threshold first. You can filter out
    low score prediction later.

    2. Go to EnzDP directory and run following command:
        python enzdp.py /full/path/to/sample_proj.py

    3. Look for logs in this file: /full/path/to/sample_proj.py.log

    4. Look for result in the specified output file
    
Result Format:
--------------
    Please refer to these files for more details: 
        /path/to/EnzDP/source/run/res.format 
    and 
        /path/to/EnzDP/source/run/site.format


Web Version:
------------
    Webversion of EnzDP is available at:
    http://biogpu.ddns.comp.nus.edu.sg/~nguyennn/wwwEnzDP/

    (Since 01-Apr-2016)

Contact:
--------
    Please feel free to contact on any issue of EnzDP to:
        nguyennn@comp.nus.edu.sg
        ninhnnsoc@gmail.com


