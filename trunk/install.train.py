
import os, sys, shutil

_SOURCE = os.path.abspath(sys.path[0])
_TARGET = sys.argv[1]

print "Target directory is [%s]" % _TARGET

FileList = ['progs', 'source', 'Swiss-prot/getIt', 
            'enzdp.train.py', 'readme.txt', 'train.readme.txt',
            'myutils.py',
            ] 

def copyanything(src, dst):
    if os.path.isfile(src):
        shutil.copy(src, dst)
    else:
        shutil.copytree(src, dst)

def copyThemAll(src, dst):   
    if not os.path.isfile(src):
        if not os.path.exists(dst):    
            os.makedirs(dst)
    os.system("cp -rf %s %s" %(src, dst))   
              
# Copy all files:
for fileOrFolder in FileList:
    source = _SOURCE + "/"+fileOrFolder
    target = _TARGET + "/"+fileOrFolder
    print "Copy [%s] to [%s]:" %(source, target) 
    #copyanything(source, target)
    #os.system("cp -rf %s %s" %(source, target))
    copyThemAll(source, target)
    print "Copy [%s] to [%s]: DONE" %(source, target)





